#!/bin/bash

for i in 0 1
do
    # don't set MAC address since that will change when VM cloned
    sed -i 's/^HWADDR/#HWADDR/' /etc/sysconfig/network-scripts/ifcfg-eth$i
    # TODO: why should this be set to no?
    sed -i 's/^NM_CONTROLLED=yes/NM_CONTROLLED=no/' /etc/sysconfig/network-scripts/ifcfg-eth$i
done 

# remove reference to MAC address since that will change when VM cloned
sed -i 's/ATTR{address[^,]*,//' /etc/udev/rules.d/70-persistent-net.rules

curl -s https://bitbucket.org/ebow/setup/raw/master/vagrant/etc-skel/.bash_profile > /etc/skel/.bash_profile
curl -s https://bitbucket.org/ebow/setup/raw/master/vagrant/etc-skel/.bashrc > /etc/skel/.bashrc
curl -s https://bitbucket.org/ebow/setup/raw/master/vagrant/etc-skel/.vimrc > /etc/skel/.vimrc

useradd vagrant
usermod -aG wheel vagrant
echo vagrant | passwd vagrant --stdin
mkdir -m 0700 ~vagrant/.ssh
curl -s https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub >> ~vagrant/.ssh/authorized_keys
chown -R vagrant:vagrant ~vagrant/
chmod -R 0600 ~vagrant/.ssh/*
echo "vagrant ALL=(ALL) ALL" >> /etc/sudoers
echo "%wheel ALL=NOPASSWD: ALL" >> /etc/sudoers
echo 'Defaults env_keep += "SSH_AUTH_SOCK"' >> /etc/sudoers
sed -i 's/Defaults\s*requiretty//' /etc/sudoers
echo "UseDNS no" >> /etc/ssh/sshd_config
