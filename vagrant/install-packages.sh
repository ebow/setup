#!/bin/bash

SETUPDIR=/tmp/
DEVOPS=vagrant

rpm -Uvh 'http://yum.postgresql.org/9.3/redhat/rhel-6-i386/pgdg-centos93-9.3-1.noarch.rpm'
rpm -Uvh 'http://fedora.mirror.nexicom.net/epel/6/i386/epel-release-6-8.noarch.rpm'
rpm -Uvh 'http://nginx.org/packages/centos/6/noarch/RPMS/nginx-release-centos-6-0.el6.ngx.noarch.rpm'

yum -y update
yum -y install gcc gcc-c++ make openssl-devel freetype fontconfig freetype-devel fontconfig-devel postgis2_93 postgis2_93-devel postgis2_93-utils postgresql93 postgresql93-devel postgresql93-libs postgresql93-server postgresql93-contrib pcre-devel screen wget zlib-devel readline-devel libxml2 libxml2-devel libxslt libxslt-devel man gd-devel cpan links lynx unzip nginx kernel-devel-`uname -r` curl-devel expat-devel gettext asciidoc xmlto mysql-server 

yum -y update kernel*
ln -s /usr/pgsql-9.3/bin/pg_config /usr/local/bin/pg_config
service postgresql-9.3 initdb
chkconfig postgresql-9.3 on
/etc/init.d/postgresql-9.3 start
#sed -i '/# "local" is for Unix domain socket connections only/ a\local   tracking        tracking                                trust' /var/lib/pgsql/9.3/data/pg_hba.conf

echo 'installing daemontools'
cd $SETUPDIR
wget -c 'http://cr.yp.to/daemontools/daemontools-0.76.tar.gz'
mkdir -p /package
chmod 1755 /package
cd /package/
gunzip $SETUPDIR/daemontools-0.76.tar.gz
tar -xpf $SETUPDIR/daemontools-0.76.tar
cd admin/daemontools-0.76/
sed -i '/gcc/ s/$/ -include \/usr\/include\/errno.h/' src/conf-cc
package/install
# TODO: remove the /etc/inittab line added for svscanboot
# create a new file /etc/init/svscan.conf to match new startup style
echo -e "start on runlevel [345]\nrespawn\nexec /command/svscanboot" > /etc/init/svscan.conf
# now, tell init, it should re-read it’s configuration, and then start
# svscanboot:
initctl reload-configuration
initctl start svscan

echo 'downloading and installing node.js v0.10.29'
su - $DEVOPS -c "cd $SETUPDIR && wget http://nodejs.org/dist/v0.10.29/node-v0.10.29.tar.gz && tar zxf node-v0.10.29.tar.gz && cd node-v0.10.29/ && ./configure && make"  && cd $SETUPDIR/node-v0.10.29 && make install 

echo 'installing node modules'
PG_CONFIG=/usr/pgsql-9.3/bin/pg_config npm install -g pg expresso request async coffee-script socket.io twilio ejs qunitjs express formidable mocha aws-sign selenium-webdriver buffer-crc32 buster generic-pool nodeunit should node-uuid highlight.js hogan.js htmlparser connect http-signature cookie inherits@2 oauth-sign uid2 cookie-jar once uid-number cookie-signature underscore jade vows jshint yql debug keypress twilio plivo-node excel fixmyjs grunt-cli expect.js jslint js-beautify sinon campaign mailin node-xmpp mailparser node-pushserver nodemailer node-inspector


# install
echo "downloading and installing perl-5.20.0 and CPAN modules"
su - $DEVOPS -c "cd $SETUPDIR && wget http://www.cpan.org/src/5.0/perl-5.20.0.tar.gz && tar zxf perl-5.20.0.tar.gz && cd perl-5.20.0/ && sh Configure -Dusethreads -des && make && make test " && cd $SETUPDIR/perl-5.20.0/ && make install && /usr/local/bin/cpan YAML LWP HTTP::Date Getopt::Long Text::CSV_XS PDF::API2 Compress::Zlib Crypt::CBC Digest::HMAC_MD5 GD GD::Barcode JSON JSON_XS HTML::Template Text::CSV DBD::SQLite DBD::Pg FCGI OLE::Storage_Lite IO::Scalar Parse::RecDescent File::Temp Spreadsheet::XLSX Spreadsheet::ParseExcel Date::Simple XML::Parser XML::DOM XML::XSLT XML::Path XML::Simple XML::Dumper XML::LibXML Linux::Inotify2 Perl::Critic Modern::Perl Module::Build Devel::Cover Devel::PPPort Test::Pod Test::Pod::Coverage Perl::Tidy Test::Valgrind Test::MinimumVersion Module::Release Test::Prereq Parse::CPAN::Meta Catalyst ExtUtils::MakeMaker Moose Dancer Mojolicious HTML::TreeBuilder HTML::TreeBuilder::XPath Test::More Dist::Zilla Module::Starter Module::Install 

echo 'downloading and installing git 2.0.1'
su - $DEVOPS -c "cd $SETUPDIR && wget https://www.kernel.org/pub/software/scm/git/git-2.0.1.tar.gz && tar zxf git-2.0.1.tar.gz && cd /tmp/git-2.0.1 && make configure && ./configure --with-libpcre --without-tcltk --with-perl=/usr/local/bin/perl && make all doc" && cd $SETUPDIR/git-2.0.1 && make install install-doc install-html 

# install emacs from source
echo 'downloading and compiling emacs 24.3'
su - $DEVOPS -c "cd $SETUPDIR && wget ftp://ftp.is.co.za/mirror/ftp.gnu.org/gnu/emacs/emacs-24.3.tar.gz && tar zxf emacs-24.3.tar.gz && cd emacs-24.3 && ./configure --without-x  && make " && cd $SETUPDIR/emacs-24.3/ && make install

cd $SETUPDIR
echo 'downloading phantomjs 1.9.2'
wget https://phantomjs.googlecode.com/files/phantomjs-1.9.2-linux-i686.tar.bz2
tar jxf phantomjs-1.9.2-linux-i686.tar.bz2
cp phantomjs-1.9.2-linux-i686/bin/phantomjs /usr/local/bin/
