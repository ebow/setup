if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi

export HISTTIMEFORMAT='%F %T '
export HISTSIZE=10000000
export NODE_PATH=/usr/local/lib/node_modules
