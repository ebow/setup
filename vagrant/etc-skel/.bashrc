if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

PS1="\[\033[1;31m\]#\[\033[0m\] \d \t \u@\h \! \# \[\033[1;31m\]\w/\[\033[0m\]\n"

export T=~/.`date +%Y/%m/%d`;

###############################################################################
# time tracking
# log project <project name> (implies work has started on an unnamed task)
# log start <task name> (implies starting work on task, task is now active)
# log stop (stopped work on active task or project)
#
# or what about log task <task name> log subtask <sub task name> so I can group
# the different sub tasks I did in solving a problem?
#
# In that case the commands to log are:
# project, task, subtask, stop, note
function log() {
    echo $(date "+%F %T") - "$@" >> $T/.log/logged
}

###############################################################################

# look back a week to see when we last worked 
for i in 24 48 72 96 120 144 168; do
    D=~/.`date -v -${i}H +%Y/%m/%d`;
    if [ -d $D ]; then
        export P=$D;
        break;
    fi
done

if [ ! -d $T ]; then
    mkdir -p $T/{.log,projects};
    if [ $P ]; then
        echo "Last worked: $P";
        cat $P/.log/bash-*.log | sort > $P/.log/history.log
        rm $P/.log/bash-*.log
        touch $T/README
        # get rid of temp files
        find $P -name '*~' -print0 | xargs -0 rm 
        find $P -name '*.swp' -print0 | xargs -0 rm 
        # unchanging log of what was done
        chmod -R ugo-w $P;
    fi
fi

cd $T
PROMPT_COMMAND='e=`date "+%F %T"`;l=$(history 1);r="^ *([0-9]+) *[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2} *([^ ]+).*";if [[ $l =~ $r ]]; then n=${BASH_REMATCH[1]}; c=${BASH_REMATCH[2]}; if [[ -z $__PREV_HISTNUM || -z $__PREV_HISTCMD ]]; then export __PREV_HISTNUM=$n; export __PREV_HISTCMD=$c; elif [[ $n != $__PREV_HISTNUM  || $c != $__PREV_HISTCMD ]]; then export __PREV_HISTNUM=$n; export __PREV_HISTCMD=$c; if [[ $c == "cd" ]]; then d=$OLDPWD; else d=$PWD; fi; log="${l#*[0-9]* *} [$e] $n $d"; echo $log >> "$T/.log/bash-`date "+%Y%m%d"`-$$.log"; fi; fi'
