"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" ward off unexpected things distro might have made, as well
" as sanely reset options when re-sourceing .vimrc
set nocompatible

" 
set textwidth=79

" Indenting (without hard tabs)
set expandtab
set shiftwidth=4
set softtabstop=4

" Attempt to determine the type of file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype plugin indent on

" Allow re-use of same window and switch from an unsaved buffer without saving
" it first.
set hidden

" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase

" Show matches as you type
set incsearch 

" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start

" Stop certain movements from always going to the first character of a line.
set nostartofline

" Instead of failing a command because of unsaved changes, instead raise a
" dialogue asking if you wish to save changed files.
set confirm

" Map <C-L> (redraw screen) to also turn off search highlighting until the
" next search.
nnoremap <C-L> :nohl<CR><C-L>

" Turn off syntax highlighting
syntax off 

" Display line numbers on the left
set number

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" define :JsTidy command to run js-beautify on visual selection or buffer
command -range=% -nargs=* JsTidy <line1>,<line2>!js-beautify -j -q -B --good-stuff -f -

" run :JsTidy on entire buffer and return cursor to (appr) original position
fun DoJsTidy()
    let Pos = line2byte( line( "." ) )
    :JsTidy
    exe "goto " . Pos
endfun
autocmd BufReadPost,FileReadPost,BufWritePre *.js,*.json :silent call DoJsTidy()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" define :PlTidy command to run perltidy on visual selection or buffer
command -range=% -nargs=* PlTidy <line1>,<line2>!perltidy -pbp -ci=2 -q

" run :PlTidy on entire buffer and return cursor to (appr) original position
fun DoPlTidy()
    let Pos = line2byte( line( "." ) )
    :PlTidy
    exe "goto " . Pos
endfun

autocmd BufReadPost,FileReadPost,BufWritePre *.p[lm] :silent call DoPlTidy()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" define :CssTidy command to run css-beautify on visual selection or buffer
command -range=% -nargs=* CssTidy <line1>,<line2>!css-beautify -s 2 -f -

" run :CssTidy on entire buffer and return cursor to (appr) original position
fun DoCssTidy()
    let Pos = line2byte( line( "." ) )
    :CssTidy
    exe "goto " . Pos
endfun
" autocmd BufReadPost,FileReadPost,BufWritePre *.css :silent call DoCssTidy()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" define :HtmlTidy command to run css-beautify on visual selection or buffer
command -range=% -nargs=* HtmlTidy <line1>,<line2>!html-beautify -s 1 -f -

" run :HtmlTidy on entire buffer and return cursor to (appr) original position
fun DoHtmlTidy()
    let Pos = line2byte( line( "." ) )
    :HtmlTidy
    exe "goto " . Pos
endfun
autocmd BufReadPost,FileReadPost,BufWritePre *.htm,*.html :silent call DoHtmlTidy()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
